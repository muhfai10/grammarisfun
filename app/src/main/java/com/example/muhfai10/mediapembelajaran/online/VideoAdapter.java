package com.example.muhfai10.mediapembelajaran.online;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebView;

import com.example.muhfai10.mediapembelajaran.R;

import java.util.List;

/**
 * Created by MuhFai10 on 1/10/2019.
 */

public class VideoAdapter extends RecyclerView.Adapter<VideoAdapter.VideoHolder> {
    List<Video> videoList ;

    public VideoAdapter(List<Video> videoList) {
        this.videoList = videoList;
    }

    @Override
    public VideoHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_video, parent, false);
        return new VideoHolder(view);
    }

    @Override
    public void onBindViewHolder(VideoHolder holder, int position) {
        holder.videoWeb.loadData(videoList.get(position).getVideoUrl(), "text/html", "utf-8");
    }

    @Override
    public int getItemCount() {
        return videoList.size();
    }

    public class VideoHolder extends RecyclerView.ViewHolder{

        WebView videoWeb;

        public VideoHolder(View itemView) {
            super(itemView);

            videoWeb = (WebView)  itemView.findViewById(R.id.webVideoView);
            videoWeb.getSettings().setJavaScriptEnabled(true);
            videoWeb.setWebChromeClient(new WebChromeClient());
        }
    }
}
