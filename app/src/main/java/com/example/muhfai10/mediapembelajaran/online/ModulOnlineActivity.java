package com.example.muhfai10.mediapembelajaran.online;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.muhfai10.mediapembelajaran.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ModulOnlineActivity extends AppCompatActivity {
    private static final String URL_VIEW_PRODUCT = "https://muhfai10.000webhostapp.com/ListTeori.php";
    private List<Teori> teoriModel;
    RecyclerView recyclerView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modul_online);
        recyclerView = (RecyclerView)findViewById(R.id.modul_list);
        recyclerView.setHasFixedSize(true);
        //final TeoriAdapter teoriAdapter = new TeoriAdapter(getApplicationContext(),teoriModel);
        recyclerView.setLayoutManager(new GridLayoutManager(getApplicationContext(),1));

        teoriModel = new ArrayList<>();

        tampilkanData();

    }

    private void tampilkanData(){
        StringRequest sb = new StringRequest(Request.Method.GET, URL_VIEW_PRODUCT, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try{
                    JSONArray jsonArray = new JSONArray(response);
                    for(int i = 0; i<jsonArray.length();i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        teoriModel.add
                                (new Teori(jsonObject.getInt("id"), jsonObject.getString("judul"), jsonObject.getString("deskripsi")
                                        ,jsonObject.getString("gambar")
                                ));

                    }
                    TeoriAdapter adapter = new TeoriAdapter(ModulOnlineActivity.this,teoriModel);
                    recyclerView.setAdapter(adapter);


                }catch (JSONException js){
                    js.printStackTrace();
                }
            }
        }, new Response.ErrorListener(){

            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        Volley.newRequestQueue(this).add(sb);
    }
}
