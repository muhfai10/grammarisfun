package com.example.muhfai10.mediapembelajaran;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.Arrays;
import java.util.List;

public class ListModulActivity extends AppCompatActivity {
ListView listModul;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_modul);
        listModul = (ListView)findViewById(R.id.listModul);
        String[] judulArray = getResources().getStringArray(R.array.judularray);
        List<String> listJudul = Arrays.asList(judulArray);
        ArrayAdapter<String> arrayJudul = new ArrayAdapter<String>(getBaseContext(),android.R.layout.simple_list_item_1,listJudul);

        listModul.setAdapter(arrayJudul);
        listModul.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String judulModul = parent.getItemAtPosition(position).toString();
                Toast.makeText(getApplicationContext(),"Judul Modul" + judulModul,Toast.LENGTH_LONG).show();
            }
        });
    }
}
