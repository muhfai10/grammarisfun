package com.example.muhfai10.mediapembelajaran.online;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.muhfai10.mediapembelajaran.R;

public class TampilModul extends AppCompatActivity {
    private TextView txtjudul,txtContent;
    private ImageView img;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tampil_modul);

        txtjudul = (TextView)findViewById(R.id.txtjudul_modul);
        img = (ImageView)findViewById(R.id.imgmodultampil);
        txtContent = (TextView)findViewById(R.id.txt_content_modul);

        // Recieve data
        Intent intent = getIntent();
        String Judul = intent.getExtras().getString("Judul");
        String Cover = intent.getExtras().getString("Cover");
        String Content = intent.getExtras().getString("Content");

        // Setting values
        txtjudul.setText(Judul);
        //img.setImageResource(Cover);
        Glide.with(this).load(Cover).into(img);
        txtContent.setText(Content);
    }
}
