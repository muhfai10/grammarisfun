package com.example.muhfai10.mediapembelajaran.online;

/**
 * Created by MuhFai10 on 1/10/2019.
 */

public class Video {

   private String videoUrl;

    public Video(String videoUrl) {
        this.videoUrl = videoUrl;
    }

    public String getVideoUrl() {
        return videoUrl;
    }
}
