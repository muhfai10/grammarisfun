package com.example.muhfai10.mediapembelajaran.kuis;

/**
 * Created by MuhFai10 on 1/9/2019.
 */

public class Quiz2 {
    private String textQuestions [] = {
            "1. Tomy, Jane, Mark, Sarah ………. smart students.",
            "2. A: Hello, my name is Alice Wong\n" +
                    "B: Hi, I’m Susan Crane.\n" +
                    "A: Sorry, ……….\n" +
                    "B: It’s C-R-A-N-E",
            "3. Are you a doctor?  No, I ……….  a dentist.",
            "4. A: Hi, his name is David Foster.\n" +
                    "B: ……….",
            "5. ………. your brother and sister ………. four languages?",
            "6. Bob’s niece is very cute. ……….name is Mia.",
            "7. Most of us……….24 SKS this semester.",
            "8. What are they doing right now?",
            "9. Irene   : What will you do on next week?\n" +
                    "Daniel: I ………. my grandmother .",
            "10. What is he doing in the garden?"
    };

    private String multipleChoice [][] = {
            {"is", "are", "am", "is not"},
            {"How do you spell your last name?", "What’s your last name?", "How do you spell your first name?", "What’s your spelling?"},
            {"am not", "don't", "am", "was"},
            {"Where are you from?", "Where do you from?", "Are you from Singapore?", "Where is he from?"},
            {"Did-speak", "Does-speak", "Do-speaks", "Do-speak"},
            {"her", "his", "he", "she"},
            {"to take", "are be taking", "are taking", "be taking"},
            {"They are doing swimming", "They swim right now", "They are swimming", "They usually go swimming"},
            {"will visiting", "visited", "am visiting", "will visit"},
            {" He is watching TV", "He is playing cards", "He is eating", "He is planting flowers"}

    };

    private  String mCorrectAnswers [] = {
            "are",
            "How do you spell your last name",
            "am",
            "Where are you from?",
            "Do-speak",
            "her",
            "are taking",
            "They are swimming",
            "will visit",
            "He is planting flowers"
    };

    public int getLength(){
        return textQuestions.length;
    }

    public String getQuestion(int a) {
        String question = textQuestions[a];
        return question;
    }

    public String getChoice(int index, int num) {
        String choice0 = multipleChoice[index][num-1];
        return choice0;
    }

    public String getCorrectAnswer(int a) {
        String answer = mCorrectAnswers[a];
        return answer;
    }
}
