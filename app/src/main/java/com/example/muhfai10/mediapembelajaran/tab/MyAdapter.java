package com.example.muhfai10.mediapembelajaran.tab;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ImageSpan;

import com.example.muhfai10.mediapembelajaran.R;
import com.example.muhfai10.mediapembelajaran.fragment.AdvancedFragment;
import com.example.muhfai10.mediapembelajaran.fragment.BasicFragment;
import com.example.muhfai10.mediapembelajaran.fragment.HomeFragment;
import com.example.muhfai10.mediapembelajaran.fragment.IntermediateFragment;

/**
 * Created by MuhFai10 on 12/30/2018.
 */

public class MyAdapter extends FragmentPagerAdapter {

    private Context context;
    //private String[] titles = {"A","B","C","D"};
    private static final int FRAGMENT_COUNT = 4;
    int[] icon = new int[]{R.drawable.home,R.drawable.number1,R.drawable.number2,R.drawable.number3};
    private int heightIcon;

    public MyAdapter(FragmentManager fm, Context c){
        super(fm);
        context = c;
        double scale = c.getResources().getDisplayMetrics().density;
        heightIcon=(int)(24*scale+0.5f);
    }

    @Override
    public Fragment getItem(int position) {
        Fragment frag= null;

        //untuk me load apabila ada di indek 0 akan berada di home dan seterusnya
        if(position ==0){
            frag = new HomeFragment();
        }else if(position == 1){
            frag = new BasicFragment();
        }else if(position == 2){
            frag = new IntermediateFragment();
        }else if(position == 3){
            frag = new AdvancedFragment();
        }

        Bundle b = new Bundle();
        b.putInt("position", position);
        frag.setArguments(b);
        return frag;
    }

    @Override
    public int getCount() {
        return FRAGMENT_COUNT;
    }

    public CharSequence getPageTitle(int position){
        switch (position){
            case 0:
                return "Home";
            case 1:
                return "Basic";
            case 2:
                return "Intermediate";
            case 3:
                return "Advanced";
        }
        return null;
    }
}
