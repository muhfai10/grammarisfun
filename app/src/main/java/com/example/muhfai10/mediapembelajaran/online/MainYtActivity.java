package com.example.muhfai10.mediapembelajaran.online;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.example.muhfai10.mediapembelajaran.R;

import java.util.Vector;

public class MainYtActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    Vector<Video> videoModel = new Vector<Video>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_yt);

        recyclerView = (RecyclerView) findViewById(R.id.rcyoutube);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

       videoModel.add(new Video("<iframe width=\"100%\" height=\"100%\" src=\"https://www.youtube.com/embed/AVYfyTvc9KY\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>"));
        videoModel.add(new Video("<iframe width=\"100%\" height=\"100%\" src=\"https://www.youtube.com/embed/Nd4MScADY94\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>"));
        videoModel.add(new Video("<iframe width=\"100%\" height=\"100%\" src=\"https://www.youtube.com/embed/lcnVMh0tR9w\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>"));
        videoModel.add(new Video("<iframe width=\"100%\" height=\"100%\" src=\"https://www.youtube.com/embed/P4wgwrAIFfA\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>"));
        videoModel.add(new Video("<iframe width=\"100%\" height=\"100%\" src=\"https://www.youtube.com/embed/ttUbJjKBncQ\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>"));


        VideoAdapter videoAdapter = new VideoAdapter(videoModel);
        recyclerView.setAdapter(videoAdapter);
    }
}
