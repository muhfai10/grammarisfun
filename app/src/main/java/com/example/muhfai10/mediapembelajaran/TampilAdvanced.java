package com.example.muhfai10.mediapembelajaran;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

public class TampilAdvanced extends AppCompatActivity {

    private TextView txtjudul,txtContent;
    private ImageView img;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tampil_advanced);

        txtjudul = (TextView)findViewById(R.id.txtjudul_adv);
        img = (ImageView)findViewById(R.id.imgadvtampil);
        txtContent = (TextView)findViewById(R.id.txt_content_adv);

        // Recieve data
        Intent intent = getIntent();
        String Judul = intent.getExtras().getString("Judul");
        int Cover = intent.getExtras().getInt("Cover");
        int Content = intent.getExtras().getInt("Content");

        // Setting values
        txtjudul.setText(Judul);
        img.setImageResource(Cover);
        txtContent.setText(Content);
    }
}
