package com.example.muhfai10.mediapembelajaran.online;

/**
 * Created by MuhFai10 on 1/8/2019.
 */

public class Teori {
    private int id;
    private String judul;
    private String deskripsi;
    private String gambar;

    public Teori(int id, String judul, String deskripsi, String gambar) {
        this.id = id;
        this.judul = judul;
        this.deskripsi = deskripsi;
        this.gambar = gambar;
    }

    public int getId() {
        return id;
    }

    public String getJudul() {
        return judul;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public String getGambar() {
        return gambar;
    }
}
