package com.example.muhfai10.mediapembelajaran.kuis;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.muhfai10.mediapembelajaran.R;

public class Quiz1Activity extends AppCompatActivity {

    private TextView mScoreView;
    private TextView mQuestionView;
    private Button btnA;
    private Button btnB;
    private Button btnC;
    private Button btnD;

    private Quiz1 mQuestionLibrary = new Quiz1();    // deklarasi kelas mm
    private String mAnswer;            // cek jawaban benar atau tidak
    private int mScore = 0; //total skor
    private int mQuestionNumber = 0; // nomor pertanyaan
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz1);

        mScoreView = (TextView)findViewById(R.id.score);
        mQuestionView = (TextView)findViewById(R.id.question);
        btnA = (Button)findViewById(R.id.choiceA);
        btnB = (Button)findViewById(R.id.choiceB);
        btnC = (Button)findViewById(R.id.choiceC);
        btnD = (Button)findViewById(R.id.choiceD);
        updateQuestion();
        updateScore(mScore);
    }

    private void updateQuestion(){
        //
        if(mQuestionNumber<mQuestionLibrary.getLength() ){
            mQuestionView.setText(mQuestionLibrary.getQuestion(mQuestionNumber));
            btnA.setText(mQuestionLibrary.getChoice(mQuestionNumber, 1));
            btnB.setText(mQuestionLibrary.getChoice(mQuestionNumber, 2));
            btnC.setText(mQuestionLibrary.getChoice(mQuestionNumber, 3));
            btnD.setText(mQuestionLibrary.getChoice(mQuestionNumber,4));
            mAnswer = mQuestionLibrary.getCorrectAnswer(mQuestionNumber);
            mQuestionNumber++;
        }
        else {
            Toast.makeText(Quiz1Activity.this, "Ayo coba lagi ~", Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(Quiz1Activity.this, ScoreQuiz1Activity.class);
            intent.putExtra("score", mScore); // pass the current score to the second screen
            startActivity(intent);
        }
    }

    private void updateScore(int point) {
        mScoreView.setText("" + mScore+"/"+mQuestionLibrary.getLength());
    }

    public void onClick(View view) {
        //all logic for all answers buttons in one method
        Button answer = (Button) view;
        // if the answer is correct, increase the score
        if (answer.getText() == mAnswer){
            mScore = mScore + 1;
            Toast.makeText(Quiz1Activity.this, "Benar!", Toast.LENGTH_SHORT).show();
        }else
            Toast.makeText(Quiz1Activity.this, "Salah!", Toast.LENGTH_SHORT).show();

        updateScore(mScore);
        updateQuestion();
    }
}
