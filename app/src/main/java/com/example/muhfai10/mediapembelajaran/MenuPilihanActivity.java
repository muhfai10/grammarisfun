package com.example.muhfai10.mediapembelajaran;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MenuPilihanActivity extends AppCompatActivity {
Button btnOnline, btnOffline;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_pilihan);
        btnOnline = (Button)findViewById(R.id.btnOnline);
        btnOffline = (Button)findViewById(R.id.btnOffline);
        btnOnline.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MenuPilihanActivity.this, NavOnlineActivity.class);
                startActivity(intent);
            }
            });
        btnOffline.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MenuPilihanActivity.this, NavigationActivity.class);
                startActivity(intent);
            }
        });
    }
}
