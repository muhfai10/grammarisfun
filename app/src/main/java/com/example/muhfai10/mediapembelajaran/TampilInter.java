package com.example.muhfai10.mediapembelajaran;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.muhfai10.mediapembelajaran.Model.BasicListModel;
import com.example.muhfai10.mediapembelajaran.Model.InterListModel;
import com.example.muhfai10.mediapembelajaran.R;
import com.example.muhfai10.mediapembelajaran.adapter.BasicAdapter;
import com.example.muhfai10.mediapembelajaran.adapter.InterAdapter;

import java.util.List;

public class TampilInter extends AppCompatActivity {

    private TextView txtjudul,txtContent;
    private ImageView img;
    InterAdapter interAdapter;
    List<InterListModel> interListModel;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tampil_inter);

        txtjudul = (TextView)findViewById(R.id.txtjudul_inter);
        img = (ImageView)findViewById(R.id.imgintertampil);
        txtContent = (TextView)findViewById(R.id.txt_content_inter);

        // Recieve data
        Intent intent = getIntent();
        String Judul = intent.getExtras().getString("Judul");
        int Cover = intent.getExtras().getInt("Cover");
        int Content = intent.getExtras().getInt("Content");

        // Setting values
        txtjudul.setText(Judul);
        img.setImageResource(Cover);
        txtContent.setText(Content);
    }
}
