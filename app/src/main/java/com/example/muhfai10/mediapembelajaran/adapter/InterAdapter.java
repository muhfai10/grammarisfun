package com.example.muhfai10.mediapembelajaran.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.muhfai10.mediapembelajaran.Model.BasicListModel;
import com.example.muhfai10.mediapembelajaran.Model.InterListModel;
import com.example.muhfai10.mediapembelajaran.R;
import com.example.muhfai10.mediapembelajaran.TampilBasic;
import com.example.muhfai10.mediapembelajaran.TampilInter;

import java.util.List;

/**
 * Created by MuhFai10 on 1/4/2019.
 */

public class InterAdapter extends RecyclerView.Adapter<InterAdapter.InterListHolder> {

    private Context context;
    private List<InterListModel> interListModel;

    public InterAdapter(Context context, List<InterListModel> interListModel) {
        this.context = context;
        this.interListModel = interListModel;
    }

    @NonNull
    @Override

    public InterListHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.view_list_inter, parent,false);

        return new InterListHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull InterListHolder holder,final int position) {
        InterListModel interModel = interListModel.get(position);
        //Glide.with(context).load(basicModel.getCover()).into(holder.basicImage);
        holder.interImage.setImageResource(interModel.getCover());
        holder.txtJudul.setText(interModel.getJudul());
        holder.interContent.setText(interModel.getContent());
        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, TampilInter.class);

                intent.putExtra("Judul",interListModel.get(position).getJudul());
                intent.putExtra("Cover",interListModel.get(position).getCover());
                intent.putExtra("Content",interListModel.get(position).getContent());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return interListModel.size();
    }

    class InterListHolder extends RecyclerView.ViewHolder{
        TextView txtJudul;
        ImageView interImage;
        TextView interContent;
        CardView cardView;
        public InterListHolder(View itemView) {
            super(itemView);
            txtJudul = (TextView) itemView.findViewById(R.id.inter_judul);
            interImage = (ImageView) itemView.findViewById(R.id.inter_cover);
            interContent = (TextView) itemView.findViewById(R.id.inter_content);
            cardView = (CardView)itemView.findViewById(R.id.cardview_id2);
        }
    }
}
