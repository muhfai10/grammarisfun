package com.example.muhfai10.mediapembelajaran;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

/**
 * Created by MuhFai10 on 12/28/2018.
 */

public class CustomAdapter extends BaseAdapter {
    Context context;
    String namaModul[];
    LayoutInflater inflater;

    public CustomAdapter(Context getApplicationContext, String namaModul[]){
        this.context = getApplicationContext;
        this.namaModul = namaModul;

        inflater = (LayoutInflater.from(getApplicationContext));
    }
    @Override
    public int getCount() {
        return 0;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return null;
    }
}
