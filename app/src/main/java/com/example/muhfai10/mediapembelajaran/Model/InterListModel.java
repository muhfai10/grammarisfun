package com.example.muhfai10.mediapembelajaran.Model;

/**
 * Created by MuhFai10 on 1/4/2019.
 */

public class InterListModel {
    private int id;
    private int cover;
    private String judul;
    private int content;

    public InterListModel(int id, int cover, String judul, int content) {
        this.id = id;
        this.cover = cover;
        this.judul = judul;
        this.content = content;
    }

    public int getId() {
        return id;
    }

    public int getCover() {
        return cover;
    }

    public String getJudul() {
        return judul;
    }

    public int getContent() {
        return content;
    }
}
