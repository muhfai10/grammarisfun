package com.example.muhfai10.mediapembelajaran.kuis;

/**
 * Created by MuhFai10 on 1/9/2019.
 */

public class Quiz1 {
    private String textQuestions [] = {
            "1. I and my friends … in the library. We read some books",
            "2. She … not work because she has the flu.",
            "3. Alina … song every night.",
            "4. My father … tea every morning.",
            "5. They … a test every week.",
            "6. Dolph: Please call me if you need.\n" +
                    "Jack: No. I … need your help.",
            "7. She is a student. She … at school.",
            "8. We … soccer match.",
            "9. Gina cooks fried rice. It … amazing.",
            "10. My brother rides a bike to school …"
    };

    private String multipleChoice [][] = {
            {"am", "is", "have", "are"},
            {"is", "does", "do", "be"},
            {"sings", "sing", "is", "does"},
            {"drink", "drinks", "drinking", "is"},
            {"does", "has", "are", "have"},
            {"do not", "does", "not", "am not"},
            {"studying", "study", "studies", "does"},
            {"doing", "watching", "watches", "watch"},
            {"does", "do", "are", "is"},
            {"everyday", "last day", "next week", "next time"}

    };

    private  String mCorrectAnswers [] = {
            "are",
            "does",
            "sings",
            "drinks",
            "have",
            "do not",
            "studies",
            "watch",
            "is",
            "everyday"
    };

    public int getLength(){
        return textQuestions.length;
    }

    public String getQuestion(int a) {
        String question = textQuestions[a];
        return question;
    }

    public String getChoice(int index, int num) {
        String choice0 = multipleChoice[index][num-1];
        return choice0;
    }

    public String getCorrectAnswer(int a) {
        String answer = mCorrectAnswers[a];
        return answer;
    }
}
