package com.example.muhfai10.mediapembelajaran.fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.muhfai10.mediapembelajaran.Model.AdvanceListModel;
import com.example.muhfai10.mediapembelajaran.Model.InterListModel;
import com.example.muhfai10.mediapembelajaran.R;
import com.example.muhfai10.mediapembelajaran.adapter.AdvanceAdapter;

import java.util.ArrayList;
import java.util.List;


public class AdvancedFragment extends Fragment {
    View v;
    RecyclerView recyclerView;

    List<AdvanceListModel> advanceListModel;
    public AdvancedFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v = inflater.inflate(R.layout.fragment_advanced, container, false);

        recyclerView = (RecyclerView) v.findViewById(R.id.advance_list);
        AdvanceAdapter advanceAdapter = new AdvanceAdapter(getContext(),advanceListModel);
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(),1));
        recyclerView.setHasFixedSize(true);

        recyclerView.setAdapter(advanceAdapter);

        return v;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        advanceListModel = new ArrayList<>();
        advanceListModel.add(new AdvanceListModel(1,R.drawable.one,"Gerunds",R.string.basic1));
        advanceListModel.add(new AdvanceListModel(2,R.drawable.two,"Gerunds VS Present Participle",R.string.basic2));
        advanceListModel.add(new AdvanceListModel(3,R.drawable.three,"Either ... Or/Neither ... Nor",R.string.basic3));
        advanceListModel.add(new AdvanceListModel(4,R.drawable.four,"Not Only ... But Also",R.string.basic4));
        advanceListModel.add(new AdvanceListModel(5,R.drawable.five,"Both ... And",R.string.basic5));
        advanceListModel.add(new AdvanceListModel(6,R.drawable.six,"Question Tag : is, am, are",R.string.basic1));
        advanceListModel.add(new AdvanceListModel(7,R.drawable.seven,"Question Tag : Do, Does ",R.string.basic1));
        advanceListModel.add(new AdvanceListModel(8,R.drawable.eight,"Present Perfect Continuous Tense",R.string.basic1));
        advanceListModel.add(new AdvanceListModel(9,R.drawable.nine,"Past Perfect Tense",R.string.basic1));
        advanceListModel.add(new AdvanceListModel(10,R.drawable.ten,"Past Perfect Continuous Tense",R.string.basic1));
    }


}
