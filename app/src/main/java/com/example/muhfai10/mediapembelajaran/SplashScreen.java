package com.example.muhfai10.mediapembelajaran;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class SplashScreen extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        Thread timer = new Thread(){
            public void run(){
                try{
                    sleep(3000);
                }catch(Exception ex){
                    ex.printStackTrace();
                }finally {
                    Intent intent = new Intent(SplashScreen.this,MenuPilihanActivity.class);
                    startActivity(intent);
                }
            }
        };
        timer.start();
    }
}
