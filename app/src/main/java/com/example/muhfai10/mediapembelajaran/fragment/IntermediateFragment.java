package com.example.muhfai10.mediapembelajaran.fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.muhfai10.mediapembelajaran.Model.BasicListModel;
import com.example.muhfai10.mediapembelajaran.Model.InterListModel;
import com.example.muhfai10.mediapembelajaran.R;
import com.example.muhfai10.mediapembelajaran.adapter.BasicAdapter;
import com.example.muhfai10.mediapembelajaran.adapter.InterAdapter;

import java.util.ArrayList;
import java.util.List;


public class IntermediateFragment extends Fragment {
    View v;
    RecyclerView recyclerView;

    List<InterListModel> interListModel;

    public IntermediateFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v = inflater.inflate(R.layout.fragment_intermediate, container, false);

        recyclerView = (RecyclerView) v.findViewById(R.id.inter_list);
        InterAdapter interAdapter = new InterAdapter(getContext(),interListModel);
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(),1));
        recyclerView.setHasFixedSize(true);

        recyclerView.setAdapter(interAdapter);

        return v;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        interListModel = new ArrayList<>();
        interListModel.add(new InterListModel(1,R.drawable.one,"Simple Past Tense",R.string.basic1));
        interListModel.add(new InterListModel(2,R.drawable.two,"Was and Were",R.string.basic2));
        interListModel.add(new InterListModel(3,R.drawable.three,"Simple Future Tense",R.string.basic3));
        interListModel.add(new InterListModel(4,R.drawable.four,"Past Future Tense",R.string.basic4));
        interListModel.add(new InterListModel(5,R.drawable.five,"Present Future Tense",R.string.basic5));
        interListModel.add(new InterListModel(6,R.drawable.six,"Since, For, Yet, and Already in Present Perfect Tense",R.string.basic1));
        interListModel.add(new InterListModel(7,R.drawable.seven,"Be Going To",R.string.basic1));
        interListModel.add(new InterListModel(8,R.drawable.eight,"Past Continuous Tense",R.string.basic1));
        interListModel.add(new InterListModel(9,R.drawable.nine,"The Correct Adjectives Order",R.string.basic1));
        interListModel.add(new InterListModel(10,R.drawable.ten,"Adjectives dengan -ed dan -ing",R.string.basic1));
    }
}
