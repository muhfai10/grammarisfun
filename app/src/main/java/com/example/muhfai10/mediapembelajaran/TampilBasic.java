package com.example.muhfai10.mediapembelajaran;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.muhfai10.mediapembelajaran.Model.BasicListModel;
import com.example.muhfai10.mediapembelajaran.adapter.BasicAdapter;

import java.util.List;

public class TampilBasic extends AppCompatActivity {
    private TextView txtjudul,txtContent;
    private ImageView img;
    BasicAdapter basicAdapter;
    List<BasicListModel>basicListModel;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tampil_basic);

       txtjudul = (TextView)findViewById(R.id.txtjudul_basic);
        img = (ImageView)findViewById(R.id.imgbasictampil);
        txtContent = (TextView)findViewById(R.id.txt_content_basic);

        // Recieve data
        Intent intent = getIntent();
        String Judul = intent.getExtras().getString("Judul");
        int Cover = intent.getExtras().getInt("Cover");
        int Content = intent.getExtras().getInt("Content");

        // Setting values
        txtjudul.setText(Judul);
        img.setImageResource(Cover);
        txtContent.setText(Content);
    }
}
