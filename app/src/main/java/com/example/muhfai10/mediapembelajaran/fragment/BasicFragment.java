package com.example.muhfai10.mediapembelajaran.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.muhfai10.mediapembelajaran.Model.BasicListModel;
import com.example.muhfai10.mediapembelajaran.R;
import com.example.muhfai10.mediapembelajaran.TampilBasic;
import com.example.muhfai10.mediapembelajaran.adapter.BasicAdapter;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class BasicFragment extends Fragment {
    View v;
    RecyclerView recyclerView;

    List<BasicListModel> basicListModel;
    public BasicFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v = inflater.inflate(R.layout.fragment_basic, container, false);

        recyclerView = (RecyclerView) v.findViewById(R.id.basic_list);
        final BasicAdapter basicAdapter = new BasicAdapter(getContext(),basicListModel);
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(),1));
        recyclerView.setHasFixedSize(true);

        recyclerView.setAdapter(basicAdapter);



        return v;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        basicListModel = new ArrayList<>();
        basicListModel.add(new BasicListModel(1,R.drawable.one,"Simple Present Tense, Part I (is, am, are)",R.string.basic1));
        basicListModel.add(new BasicListModel(2,R.drawable.two,"Simple Present Tense Part II", R.string.basic2));
        basicListModel.add(new BasicListModel(3,R.drawable.three,"Have and Has",R.string.basic3));
        basicListModel.add(new BasicListModel(4,R.drawable.four,"Demonstrative",R.string.basic4));
        basicListModel.add(new BasicListModel(5,R.drawable.five,"This and These",R.string.basic5));
        basicListModel.add(new BasicListModel(6,R.drawable.six,"That and Those",R.string.basic6));
        basicListModel.add(new BasicListModel(7,R.drawable.seven,"There is and There are",R.string.basic7));
        basicListModel.add(new BasicListModel(8,R.drawable.eight,"Noun Phrase",R.string.basic8));
        basicListModel.add(new BasicListModel(9,R.drawable.nine,"Quantifier",R.string.basic1));
        basicListModel.add(new BasicListModel(10,R.drawable.ten,"Some and Any",R.string.basic1));
    }
    /*<BasicListModel> basicList = new ArrayList<BasicListModel>();
        basicList.add(new BasicListModel("", "Mantap"));

        return basicList;*/
}
    /*private void tampilkanProduct(){
        StringRequest sb = new StringRequest(Request.Method.GET, URL_VIEW_PRODUCT, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try{
                    JSONArray jsonArray = new JSONArray(response);
                    for(int i = 0; i<jsonArray.length();i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        basicListModel.add
                                (new BasicListModel(jsonObject.getString("cover"), jsonObject.getString("judul")
                                ));

                    }
                    BasicAdapter adapter = new BasicAdapter(BasicFragment.this,basicListModel);
                    recyclerView.setAdapter(adapter);


                }catch (JSONException js){
                    js.printStackTrace();
                }
            }
        }, new Response.ErrorListener(){

            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        Volley.newRequestQueue(this).add(sb);
    }*/

