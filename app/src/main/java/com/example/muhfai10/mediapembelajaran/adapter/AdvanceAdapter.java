package com.example.muhfai10.mediapembelajaran.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.muhfai10.mediapembelajaran.Model.AdvanceListModel;
import com.example.muhfai10.mediapembelajaran.R;
import com.example.muhfai10.mediapembelajaran.TampilAdvanced;
import com.example.muhfai10.mediapembelajaran.TampilBasic;

import java.util.List;

/**
 * Created by MuhFai10 on 1/5/2019.
 */

public class AdvanceAdapter extends RecyclerView.Adapter<AdvanceAdapter.AdvanceListHolder>{

    private Context context;
    private List<AdvanceListModel> advListModel;

    public AdvanceAdapter(Context context, List<AdvanceListModel> advListModel) {
        this.context = context;
        this.advListModel = advListModel;
    }

    @Override
    public AdvanceListHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.view_list_advanced, parent,false);

        return new AdvanceListHolder(view);
    }

    @Override
    public void onBindViewHolder(AdvanceListHolder holder, final int position) {
        final AdvanceListModel advModel = advListModel.get(position);
        //Glide.with(context).load(basicModel.getCover()).into(holder.basicImage);
        holder.advImage.setImageResource(advModel.getCover());
        holder.txtJudul.setText(advModel.getJudul());
        holder.advContent.setText(advModel.getContent());
        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, TampilAdvanced.class);

                intent.putExtra("Judul",advListModel.get(position).getJudul());
                intent.putExtra("Cover",advListModel.get(position).getCover());
                intent.putExtra("Content",advListModel.get(position).getContent());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return advListModel.size();
    }

    class AdvanceListHolder extends RecyclerView.ViewHolder{
        TextView txtJudul;
        ImageView advImage;
        TextView advContent;
        CardView cardView;
        public AdvanceListHolder(View itemView) {
            super(itemView);
            txtJudul = (TextView) itemView.findViewById(R.id.adv_judul);
            advImage = (ImageView) itemView.findViewById(R.id.adv_cover);
            advContent = (TextView) itemView.findViewById(R.id.advanced_content);
            cardView = (CardView)itemView.findViewById(R.id.cardview_id3);
        }
    }
}
