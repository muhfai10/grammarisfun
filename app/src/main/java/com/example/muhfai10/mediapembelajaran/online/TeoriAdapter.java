package com.example.muhfai10.mediapembelajaran.online;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.muhfai10.mediapembelajaran.Model.InterListModel;
import com.example.muhfai10.mediapembelajaran.R;
import com.example.muhfai10.mediapembelajaran.adapter.InterAdapter;

import java.util.List;

/**
 * Created by MuhFai10 on 1/8/2019.
 */

public class TeoriAdapter extends RecyclerView.Adapter<TeoriAdapter.TeoriListHolder> {

    private Context context;
    private List<Teori> teoriModel;

    public TeoriAdapter(Context context, List<Teori> teoriModel) {
        this.context = context;
        this.teoriModel = teoriModel;
    }

    @Override
    public TeoriListHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.view_modul_online, parent,false);

        return new TeoriAdapter.TeoriListHolder(view);
    }

    @Override
    public void onBindViewHolder(TeoriListHolder holder,final int position) {
        Teori teori = teoriModel.get(position);
        Glide.with(context).load(teori.getGambar()).into(holder.teoriGambar);
        //holder.teoriGambar.setImageResource(teori.getGambar());
        holder.txtJudul.setText(teori.getJudul());
        holder.teoriDeskripsi.setText(teori.getDeskripsi());
        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, TampilModul.class);

                intent.putExtra("Judul",teoriModel.get(position).getJudul());
                intent.putExtra("Cover",teoriModel.get(position).getGambar());
                intent.putExtra("Content",teoriModel.get(position).getDeskripsi());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return teoriModel.size();
    }

    class TeoriListHolder extends RecyclerView.ViewHolder{
        TextView txtJudul;
        ImageView teoriGambar;
        TextView teoriDeskripsi;
        CardView cardView;
        public TeoriListHolder(View itemView) {
            super(itemView);
            txtJudul = (TextView) itemView.findViewById(R.id.modul_judul);
            teoriGambar = (ImageView) itemView.findViewById(R.id.image_modul);
            teoriDeskripsi = (TextView) itemView.findViewById(R.id.modul_deskripsi);
            cardView = (CardView)itemView.findViewById(R.id.cardview_id4);
        }
    }
}
