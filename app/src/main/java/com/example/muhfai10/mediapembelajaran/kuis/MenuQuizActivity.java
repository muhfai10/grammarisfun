package com.example.muhfai10.mediapembelajaran.kuis;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.muhfai10.mediapembelajaran.R;

public class MenuQuizActivity extends AppCompatActivity {
    Button btnquiz1, btnquiz2,btnquiz3;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_quiz);

        btnquiz1 = (Button)findViewById(R.id.btnkuis1);
        btnquiz2 = (Button)findViewById(R.id.btnkuis2);
        btnquiz3 = (Button)findViewById(R.id.btnkuis3);

        btnquiz1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MenuQuizActivity.this, Quiz1Activity .class);
                startActivity(intent);
            }
        });
        btnquiz2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MenuQuizActivity.this, Quiz2Activity .class);
                startActivity(intent);
            }
        });
        btnquiz3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MenuQuizActivity.this, Quiz1Activity .class);
                startActivity(intent);
            }
        });

    }
}
