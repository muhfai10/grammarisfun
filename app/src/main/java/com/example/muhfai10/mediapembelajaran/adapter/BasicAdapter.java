package com.example.muhfai10.mediapembelajaran.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.muhfai10.mediapembelajaran.Model.BasicListModel;
import com.example.muhfai10.mediapembelajaran.R;
import com.example.muhfai10.mediapembelajaran.TampilBasic;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by MuhFai10 on 1/1/2019.
 */

public class BasicAdapter extends RecyclerView.Adapter<BasicAdapter.BasicListHolder> {
    private Context context;
    private List<BasicListModel> basicListModel;
    /*private OnItemClickListener mListener;
    public void setOnItemClickListener(OnItemClickListener listener){
        mListener=listener;
    }*/
    public BasicAdapter(Context context, List<BasicListModel> basicListModel) {
        this.context = context;
        this.basicListModel = basicListModel;
    }

    /*
    public interface OnItemClickListener{
        void onItemClick(int position);
    }
    */
    @NonNull
    @Override
    public BasicListHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.view_list_basic, parent,false);

        return new BasicListHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull BasicListHolder holder, final int position) {
        BasicListModel basicModel = basicListModel.get(position);
        //Glide.with(context).load(basicModel.getCover()).into(holder.basicImage);
        holder.basicImage.setImageResource(basicModel.getCover());
        holder.txtJudul.setText(basicModel.getJudul());
        holder.basicContent.setText(basicModel.getContent());
        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, TampilBasic.class);

                intent.putExtra("Judul",basicListModel.get(position).getJudul());
                intent.putExtra("Cover",basicListModel.get(position).getCover());
                intent.putExtra("Content",basicListModel.get(position).getContent());
                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return basicListModel.size();
    }
    class BasicListHolder extends RecyclerView.ViewHolder{
        TextView txtJudul;
        ImageView basicImage;
        TextView basicContent;
        CardView cardView;
        public BasicListHolder(View itemView) {
            super(itemView);
            txtJudul = (TextView) itemView.findViewById(R.id.basic_judul);
            basicImage = (ImageView) itemView.findViewById(R.id.basic_cover);
            basicContent = (TextView) itemView.findViewById(R.id.basic_content);
            cardView = (CardView)itemView.findViewById(R.id.cardview_id);

        }
    }
}
