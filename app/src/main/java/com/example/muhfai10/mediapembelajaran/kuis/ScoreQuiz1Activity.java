package com.example.muhfai10.mediapembelajaran.kuis;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.muhfai10.mediapembelajaran.R;

public class ScoreQuiz1Activity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_score_quiz1);

        TextView txtScore = (TextView) findViewById(R.id.textScore);
        TextView txtHighScore = (TextView) findViewById(R.id.textHighScore);
        Button btncobalagi = (Button) findViewById(R.id.btncobalagi);
        btncobalagi.setOnClickListener(this);

        Intent intent = getIntent();
        int score = intent.getIntExtra("score", 0);
        txtScore.setText("Your score : " + score);

        SharedPreferences mypref = getPreferences(MODE_PRIVATE);
        int highscore = mypref.getInt("highscore", 0);
        if(highscore>= score)
            txtHighScore.setText("High score : "+highscore);
        else
        {
            txtHighScore.setText("New highscore: "+score);
            SharedPreferences.Editor editor = mypref.edit();
            editor.putInt("highscore", score);
            editor.commit();
        }
    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent(ScoreQuiz1Activity.this, Quiz1Activity.class);
        startActivity(intent);
    }

}
